module DataBaseTest

go 1.19

require (
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/miekg/pkcs11 v1.1.1
)

require github.com/go-sql-driver/mysql v1.7.0 // indirect
