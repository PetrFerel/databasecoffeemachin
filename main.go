package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"html/template"
	"net/http"
)

type Coffee struct {
	Id         int     `json:"id"`
	Marka      string  `json:"marka"`
	TypeCoffee string  `json:"type_coffee"`
	Price      float64 `json:"price"`
	Volume     float64 `json:"volume"`
}

var database *sql.DB

func DatabaseConnect(w http.ResponseWriter, r *http.Request) {
	row, err := database.Query("SELECT  * FROM  coffee")
	if err != nil {
		panic(err)
	}
	defer row.Close()
	coffees := []Coffee{}
	for row.Next() {
		var coff Coffee
		err = row.Scan(&coff.Id, &coff.Marka, &coff.TypeCoffee, &coff.Price, &coff.Volume)
		if err != nil {
			panic(err)
		}
		coffees = append(coffees, coff)
	}
	tmpl, _ := template.ParseFiles("templates/index.html")
	tmpl.Execute(w, coffees)
	//fmt.Printf("Coffee:\nPrice:%v  Marka:%s", coff.Price, coff.Marka)
	//fmt.Printf("Coffee\n Id:%v", coff)
}
func main() {
	db, err := sql.Open("mysql", "root@tcp(127.0.0.1:3306)/coffeeMachinDB")
	if err != nil {
		panic(err)
	}
	database = db
	defer db.Close()
	http.HandleFunc("/", DatabaseConnect)

	fmt.Println("Server is Liseten..")
	http.ListenAndServe("localhost:8080", nil)
}

//update, err := db.Exec("UPDATE coffeeMachinDB.coffee SET marka = ?  WHERE id = ?", `Jardin`, 3)
//if err != nil {
//	panic(err)
//}
//fmt.Println(update.LastInsertId())

//delite, err := db.Exec("DELETE  FROM coffeeMachinDB.coffee WHERE id = ?", 3)
//if err != nil {
//	panic(err)
//}
//fmt.Println(delite.LastInsertId())
//
//insert, err := db.Query("INSERT INTO coffee (`marka`, `type_coffee`, `price`, `volume`) VALUES ('ORIO', '(ARABIKA 100%)', 850, 0.250)")
//if err != nil {
//	panic(err)
//}
//defer insert.Close()
//fmt.Println("Done")
